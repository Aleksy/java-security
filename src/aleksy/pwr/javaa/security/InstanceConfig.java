package aleksy.pwr.javaa.security;

public class InstanceConfig {
   private int maxCapacity;
   private int maxItemsNumber;
   private int maxItemWeight;

   private InstanceConfig(int maxCapacity, int maxItemsNumber, int maxItemWeight) {
      this.maxCapacity = maxCapacity;
      this.maxItemsNumber = maxItemsNumber;
      this.maxItemWeight = maxItemWeight;
   }

   /**
    * Getter for max capacity
    *
    * @return max capacity
    */
   public int getMaxCapacity() {
      return maxCapacity;
   }

   /**
    * Getter for max items number
    *
    * @return max number of items
    */
   public int getMaxItemsNumber() {
      return maxItemsNumber;
   }

   /**
    * Getter for max item weight
    *
    * @return max weight of item
    */
   public int getMaxItemWeight() {
      return maxItemWeight;
   }

   /**
    * Creates default configurations for {@link KnapsackProblemInstance} generator
    *
    * @return default config object
    */
   public static InstanceConfig createDefault() {
      return new InstanceConfig(
         InstanceConfigConstants.DEFAULT_MAX_CAPACITY,
         InstanceConfigConstants.DEFAULT_MAX_NUMBER_OF_ITEMS,
         InstanceConfigConstants.DEFAULT_MAX_ITEM_WEIGHT);
   }

   /**
    * Checks a vaildity of given data and creates new instance of {@link InstanceConfig}.
    * For {@code null} or lower or equal 0 parameters takes default values from {@link InstanceConfigConstants}.
    * If you would like to create instance with custom maximal capacity and default other values you can
    * give capacity and {@code nulls} as parameters.
    *
    * @param maxCapacity    maximal capacity to set
    * @param maxItemsNumber maximal number of items to set
    * @param maxItemWeight  maximal item weight to set
    * @return new instance of {@link InstanceConfig}
    */
   public static InstanceConfig create(Integer maxCapacity, Integer maxItemsNumber, Integer maxItemWeight) {
      if (maxCapacity == null || maxCapacity <= 0)
         maxCapacity = InstanceConfigConstants.DEFAULT_MAX_CAPACITY;
      if (maxItemsNumber == null || maxItemsNumber <= 0)
         maxItemsNumber = InstanceConfigConstants.DEFAULT_MAX_NUMBER_OF_ITEMS;
      if (maxItemWeight == null || maxItemWeight <= 0)
         maxItemWeight = InstanceConfigConstants.DEFAULT_MAX_ITEM_WEIGHT;
      return new InstanceConfig(maxCapacity, maxItemsNumber, maxItemWeight);
   }
}
