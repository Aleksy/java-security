package aleksy.pwr.javaa.security;

public class SecurityApp {
   public static void main(String[] args) throws Exception {
      if (args.length == 0) {
         System.out.println("No args. Available args:");
         System.out.println("rsa - file encrypting and decrypting");
         System.out.println("knps - to solve a knapsack problem");
         return;
      }
      if(args[0].equals("rsa")) {
         Rsa.run();
      }
      if(args[0].equals("knps")) {
         Knps.run();
      }
   }
}
