package aleksy.pwr.javaa.security;

import aleksy.pwr.javaa.knapsacklib.common.model.ResultKnapsack;
import aleksy.pwr.javaa.knapsacklib.configuration.impl.RandomSearchConfig;
import aleksy.pwr.javaa.knapsacklib.logic.api.Algorithm;
import aleksy.pwr.javaa.knapsacklib.logic.impl.BruteForceAlgorithm;
import aleksy.pwr.javaa.knapsacklib.logic.impl.GreedyAlgorithm;
import aleksy.pwr.javaa.knapsacklib.logic.impl.RandomSearchAlgorithm;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

public class Knps {
   private static List<Algorithm> algorithms = assign();

   public static void run() throws IOException {
//      System.out.println("Checking if the library is signed.");
//      checkSigned();
      System.out.println("Generating an instance.");
      Random random = new Random();
      KnapsackProblemInstance instance = KnapsackProblemInstances.generate(random.nextInt(), InstanceConfig.create(
         InstanceConfigConstants.DEFAULT_MAX_CAPACITY,
         InstanceConfigConstants.DEFAULT_MAX_NUMBER_OF_ITEMS,
         InstanceConfigConstants.DEFAULT_MAX_ITEM_WEIGHT));
      System.out.println("Choosing an algorithm.");
      Algorithm algorithm = chooseAlgorithm(random);
      assert algorithm != null;
      System.out.println("Chosen: " + algorithm.getDescription().getTitle());
      System.out.println("Solving");
      ResultKnapsack solved = algorithm.process(
         instance.getItems(),
         instance.getEmptyKnapsack(),
         new RandomSearchConfig(100));
      System.out.println("Result:");
      System.out.println(solved);
   }

   private static void checkSigned() throws IOException {
      JarFile jar = new JarFile(new File("F:\\Pulpit\\Studia\\Polibuda\\Semestr VI" +
         "\\Javaa\\java_security\\Knapsack Library 0.3.0\\javaa-knapsack-library-0.3.0-signed"));

      InputStream is = jar.getInputStream(jar.getEntry("META-INF/MANIFEST.MF"));
      Manifest man = new Manifest(is);
      is.close();

      Set<String> signed = new HashSet<>();
      for(Map.Entry<String, Attributes> entry: man.getEntries().entrySet()) {
         for(Object attrkey: entry.getValue().keySet()) {
            if (attrkey instanceof Attributes.Name &&
               attrkey.toString().contains("-Digest"))
               signed.add(entry.getKey());
         }
      }

      Set<String> entries = new HashSet<>();
      for(Enumeration<JarEntry> entry = jar.entries(); entry.hasMoreElements(); ) {
         JarEntry je = entry.nextElement();
         if (!je.isDirectory())
            entries.add(je.getName());
      }
      Set<String> unsigned = new HashSet<>(entries);
      unsigned.removeAll(signed);
      Set<String> missing = new HashSet<>(signed);
      missing.removeAll(entries);
   }

   private static List<Algorithm> assign() {
      return Arrays.asList(new GreedyAlgorithm(), new BruteForceAlgorithm(), new RandomSearchAlgorithm());
   }


   private static Algorithm chooseAlgorithm(Random random) {
      if (algorithms.isEmpty())
         return null;
      int index = random.nextInt(algorithms.size());
      return algorithms.get(index);
   }
}
