package aleksy.pwr.javaa.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class FileUtil {
   public static byte[] getFileInBytes(File file) throws IOException {
      FileInputStream fis = new FileInputStream(file);
      byte[] fbytes = new byte[(int) file.length()];
      fis.read(fbytes);
      fis.close();
      return fbytes;
   }
}
