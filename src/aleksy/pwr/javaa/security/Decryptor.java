package aleksy.pwr.javaa.security;

import javax.crypto.Cipher;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Scanner;

public class Decryptor {
   public static void run(Scanner scanner) throws Exception {
      System.out.print("Enter a filename to decrypt: ");
      String file = scanner.nextLine();
      System.out.print("Enter a public key location: ");
      String pub = scanner.nextLine();
      System.out.print("Enter a destination filename: ");
      String dest = scanner.nextLine();
      process(new File(file), new File(pub), new File(dest));
   }

   private static void process(File toDecr, File pubKey, File dest) throws Exception {
      Cipher cipher = Cipher.getInstance("RSA");
      System.out.println("Reading a file with public key.");
      cipher.init(Cipher.DECRYPT_MODE, getKey(pubKey));
      System.out.println("Reading a message to decrypt.");
      String messageToDecrypt = getMessage(toDecr);
      System.out.println("Decrypting a message.");
      String decoded = new String(cipher.doFinal(Base64.getMimeDecoder().decode(messageToDecrypt)));
      System.out.println("Saving the decoded message to file.");
      saveToFile(dest, decoded);
      System.out.println("Decoded message:");
      System.out.println(decoded);
   }

   private static void saveToFile(File file, String toSave) throws IOException {
      file.createNewFile();
      FileWriter fileWriter = new FileWriter(file);
      fileWriter.write(toSave);
      fileWriter.close();
   }

   private static String getMessage(File file) throws FileNotFoundException {
      StringBuilder builder = new StringBuilder();
      Scanner scanner = new Scanner(file);
      while (scanner.hasNextLine()) {
         builder.append(scanner.nextLine()).append("\r\n");
      }
      scanner.close();
      return builder.toString();
   }

   private static PublicKey getKey(File file) throws Exception {
      byte[] keyBytes = Files.readAllBytes(file.toPath());
      X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
      KeyFactory kf = KeyFactory.getInstance("RSA");
      return kf.generatePublic(spec);
   }
}
