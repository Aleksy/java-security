package aleksy.pwr.javaa.security;

public class InstanceConfigConstants {
   public static final int DEFAULT_MAX_CAPACITY = 100;
   public static final int DEFAULT_MAX_NUMBER_OF_ITEMS = 20;
   public static final int DEFAULT_MAX_ITEM_WEIGHT = 50;

}
