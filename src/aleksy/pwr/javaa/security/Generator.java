package aleksy.pwr.javaa.security;

import java.io.*;
import java.security.*;
import java.util.Scanner;

public class Generator {
   public static void run(Scanner scanner) throws NoSuchAlgorithmException, IOException {
      System.out.print("Enter a file basename: ");
      String filename = scanner.nextLine();
      File priv = new File(filename + "_priv.txt");
      File pub = new File(filename + "_pub.txt");
      System.out.print("Enter a key length: ");
      int keyLength = Integer.parseInt(scanner.nextLine());
      process(priv, pub, keyLength);
   }

   private static void process(File priv, File pub, int keyLength) throws NoSuchAlgorithmException, IOException {
      System.out.println("Initializing a key generator.");
      KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
      keyPairGenerator.initialize(keyLength);
      System.out.println("Generating the key pair.");
      KeyPair keyPair = keyPairGenerator.generateKeyPair();
      PrivateKey privateKey = keyPair.getPrivate();
      PublicKey publicKey = keyPair.getPublic();
      System.out.println("Saving the private key to the file " + priv);
      saveToFile(priv, privateKey.getEncoded());
      System.out.println("Saving the public key to the file " + pub);
      saveToFile(pub, publicKey.getEncoded());
   }

   private static void saveToFile(File file, byte[] key) throws IOException {
      file.createNewFile();
      FileOutputStream fileOutputStream = new FileOutputStream(file);
      fileOutputStream.write(key);
      fileOutputStream.flush();
      fileOutputStream.close();
   }
}
