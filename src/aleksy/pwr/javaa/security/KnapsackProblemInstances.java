package aleksy.pwr.javaa.security;

import aleksy.pwr.javaa.knapsacklib.common.model.Item;
import aleksy.pwr.javaa.knapsacklib.common.model.Knapsack;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Util class for {{@link KnapsackProblemInstance}}
 */
public class KnapsackProblemInstances {
   /**
    * Generates a {@link KnapsackProblemInstance} by given seed. One seed given as parameter can
    * be transformed each time to the same instance. Properties of problem instances can be modified
    * by {@link InstanceConfig} object
    *
    * @param seed           for random generator
    * @param instanceConfig configuration instance
    * @return instance of knapsack problem
    */
   public static KnapsackProblemInstance generate(long seed, InstanceConfig instanceConfig) {
      Random random = new Random(seed);
      int capacity = random.nextInt(instanceConfig.getMaxCapacity());
      int itemsNumber = random.nextInt(instanceConfig.getMaxItemsNumber());
      List<Item> items = new ArrayList<>();
      for (int i = 0; i < itemsNumber; i++) {
         items.add(new Item(random.nextDouble(), random.nextInt(instanceConfig.getMaxItemWeight())));
      }
      return new KnapsackProblemInstance(new Knapsack(capacity), items, seed);
   }
}