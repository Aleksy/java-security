package aleksy.pwr.javaa.security;

import aleksy.pwr.javaa.knapsacklib.common.model.Item;
import aleksy.pwr.javaa.knapsacklib.common.model.Knapsack;

import java.util.List;

/**
 * Instance of knapsack problem
 */
public class KnapsackProblemInstance {
   private Knapsack emptyKnapsack;
   private List<Item> items;
   private long seed;

   public KnapsackProblemInstance(Knapsack emptyKnapsack, List<Item> items, long seed) {
      this.emptyKnapsack = emptyKnapsack;
      this.items = items;
      this.seed = seed;
   }

   /**
    * Getter for empty knapsack
    *
    * @return empty knapsack
    */
   public Knapsack getEmptyKnapsack() {
      return emptyKnapsack;
   }

   /**
    * Getter for list of items
    *
    * @return items
    */
   public List<Item> getItems() {
      return items;
   }

   /**
    * Seed which was used to generate the instance
    *
    * @return seed
    */
   public long getSeed() {
      return seed;
   }
}
