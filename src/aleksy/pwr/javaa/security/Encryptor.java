package aleksy.pwr.javaa.security;

import javax.crypto.Cipher;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Scanner;

public class Encryptor {
   public static void run(Scanner scanner) throws Exception {
      System.out.print("Enter a filename to encrypt: ");
      String file = scanner.nextLine();
      System.out.print("Enter a private key location: ");
      String priv = scanner.nextLine();
      System.out.print("Enter a destination filename: ");
      String dest = scanner.nextLine();
      process(new File(file), new File(priv), new File(dest));
   }

   private static void process(File toEncr, File privKey, File dest) throws Exception {
      Cipher cipher = Cipher.getInstance("RSA");
      System.out.println("Reading a file with private key.");
      cipher.init(Cipher.ENCRYPT_MODE, getKey(privKey));
      System.out.println("Reading a message to encrypt.");
      String messageToEncrypt = getMessage(toEncr);
      System.out.println("Encrypting a message.");
      String encoded = Base64.getEncoder()
         .encodeToString(cipher.doFinal(messageToEncrypt.getBytes("UTF-8")));
      System.out.println("Saving the encoded message to file.");
      saveToFile(dest, encoded);
      System.out.println("Encoded message:");
      System.out.println(encoded);
   }

   private static void saveToFile(File file, String toSave) throws IOException {
      FileWriter fileWriter = new FileWriter(file);
      fileWriter.write(toSave);
      fileWriter.close();
   }

   private static String getMessage(File file) throws FileNotFoundException {
      StringBuilder builder = new StringBuilder();
      Scanner scanner = new Scanner(file);
      while (scanner.hasNextLine()) {
         builder.append(scanner.nextLine()).append("\r\n");
      }
      scanner.close();
      return builder.toString();
   }

   private static PrivateKey getKey(File file) throws Exception {
      byte[] keyBytes = Files.readAllBytes(file.toPath());
      PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
      KeyFactory kf = KeyFactory.getInstance("RSA");
      return kf.generatePrivate(spec);
   }
}
