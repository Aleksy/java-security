package aleksy.pwr.javaa.security;

import java.util.Scanner;

public class Rsa {
   public static void run() throws Exception {
      System.out.println("ENCRYPTING / DECRYPTING");
      System.out.println("Options: ");
      System.out.println("g - generate key pair");
      System.out.println("e - encrypt a file");
      System.out.println("d - decrypt a file");
      System.out.println("exit - exit a program");
      Scanner scanner = new Scanner(System.in);
      while (true) {
         System.out.print(":> ");
         String input = scanner.nextLine();
         if (input.equals("exit")) break;

         if (input.equals("g")) Generator.run(scanner);
         else if (input.equals("e")) Encryptor.run(scanner);
         else if (input.equals("d")) Decryptor.run(scanner);
      }
      scanner.close();
   }
}
